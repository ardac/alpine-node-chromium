FROM alpine:3.8

# install nodejs, npm and chromium
RUN apk add --no-cache \
    nodejs=8.11.4-r0 \
    npm=8.11.4-r0 \
    chromium
